#!/bin/bash

set -e


rm -rf constant/polyMesh
blockMesh
snappyHexMesh -overwrite
checkMesh
transformPoints -scale '(0.01 0.01 0.01)'

